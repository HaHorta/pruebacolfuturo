package co.com.colfuturo.pruebaColfuturo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PruebaColfuturoApplication {

	public static void main(String[] args) {
		SpringApplication.run(PruebaColfuturoApplication.class, args);
	}

}
