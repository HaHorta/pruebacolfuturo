import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule, HttpClient } from '@angular/common/http';

//service
import { ApiBase } from '../app/infrastructura/api-base.service';

//component
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MenuColfuturoComponent } from './componentes/menu-colfuturo/menu-colfuturo.component';
import { FormularioColfuturoComponent } from './componentes/formulario-colfuturo/formulario-colfuturo.component';
import { InicioComponent } from './componentes/inicio/inicio.component';
import { MenuLateralComponent } from './componentes/menu-lateral/menu-lateral.component';
import { PiePaginaComponent } from './componentes/pie-pagina/pie-pagina.component';
import { CartarPatrocinioComponent } from './componentes/cartar-patrocinio/cartar-patrocinio.component';
import { IdiomaComponent } from './componentes/idioma/idioma.component';

@NgModule({
  declarations: [
    AppComponent,
    MenuColfuturoComponent,
    FormularioColfuturoComponent,
    InicioComponent,
    MenuLateralComponent,
    PiePaginaComponent,
    CartarPatrocinioComponent,
    IdiomaComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule
  ],
  providers: [ApiBase],
  bootstrap: [AppComponent]
})
export class AppModule { }
