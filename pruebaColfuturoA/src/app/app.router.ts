import { Routes, RouterModule } from '@angular/router';
import {InicioComponent } from './componentes/inicio/inicio.component'

const APP_ROUTES: Routes = [
  
       { path: '**', component:InicioComponent }
];

export const APP_ROUTING = RouterModule.forRoot(APP_ROUTES, {useHash:true});