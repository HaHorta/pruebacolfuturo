import { Component, OnInit } from '@angular/core';
import { ApiColfuturoService } from '../../infrastructura/api-colfuturo.service';
import { Persona } from '../model/persona'

@Component({
  selector: 'app-formulario-colfuturo',
  templateUrl: './formulario-colfuturo.component.html',
  styleUrls: ['./formulario-colfuturo.component.css']
})
export class FormularioColfuturoComponent implements OnInit {
    persona: Persona = new Persona();

  constructor(private ApiColfuturoService: ApiColfuturoService) { }

  ngOnInit() {
  }

  generarDocumento(
        nombres: string,
        apellidos: string,
        cargo: string,
        correo: string,
        
        direccion: string,
       
        ciudad: string,
        pais: string
  ){
    this.persona.nombres = nombres;
    this.persona.apellidos = apellidos;
    this.persona.cargo = cargo;
    this.persona.correo = correo;
   
    this.persona.direccion = direccion;
   
    this.persona.ciudad = ciudad;
    this.persona.pais = pais;
    console.log("Json que llega",JSON.stringify(this.persona))
    this.ApiColfuturoService.generarDocumento(JSON.stringify(this.persona)).subscribe(data => {
    console.log(data)
    }) 
  }
}
