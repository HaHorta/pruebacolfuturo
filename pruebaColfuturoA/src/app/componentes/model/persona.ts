export class Persona {
        nombres: string;
        apellidos: string;
        cargo: string;
        correo: string;
        direccion: string;
        ciudad: string;
        pais: string;
    }