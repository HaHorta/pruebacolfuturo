import { Observable, of } from 'rxjs';
import { HttpClient, HttpHeaders, HttpHeaderResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable()
export class ApiBase {

  constructor(protected _http: HttpClient) {

  }

  public get(endpoint: string, payload = {}): Observable<any> {
    return this._http.get(endpoint, payload);
  }

  public post(endpoint: string, payload = {}): Observable<any> {
    let headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Cache-Control': 'no-cache'
    });
    return this._http.post(endpoint, payload, { headers });
  }


}