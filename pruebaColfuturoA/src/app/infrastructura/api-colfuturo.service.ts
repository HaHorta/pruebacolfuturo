import { Injectable } from '@angular/core';
import { ApiBase } from './api-base.service';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ApiColfuturoService {

  constructor(private apiBase: ApiBase) { }
 
  generarDocumento(payload: any){
    return this.apiBase.post(environment.generarDocumento, payload);
  }

}

