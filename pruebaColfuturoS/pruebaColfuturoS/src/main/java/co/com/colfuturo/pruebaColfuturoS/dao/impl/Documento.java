package co.com.colfuturo.pruebaColfuturoS.dao.impl;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;

import org.springframework.stereotype.Component;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chapter;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Font;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfWriter;

import co.com.colfuturo.pruebaColfuturoS.dao.DocumentoDAO;
import co.com.colfuturo.pruebaColfuturoS.dto.PersonaDTO;
import co.com.colfuturo.pruebaColfuturoS.utils.EnviarCorreo;
import co.com.colfuturo.pruebaColfuturoS.utils.PropiedadesConfiguracion;

@Component
public class Documento implements DocumentoDAO {

	private static final Font chapterFont = FontFactory.getFont(FontFactory.HELVETICA, 26, Font.BOLDITALIC);
	private static final Font paragraphFont = FontFactory.getFont(FontFactory.HELVETICA, 12, Font.NORMAL);

	public boolean generarDocumento(PersonaDTO persona) {
		boolean blnArchivoGenerado = false;
		EnviarCorreo ec= new EnviarCorreo();
		try {
			PropiedadesConfiguracion pc = new PropiedadesConfiguracion();
			Document document = new Document();
			PdfWriter.getInstance(document, new FileOutputStream(pc.getProperties("archivo")));

			document.open();
			document.addTitle("Carta aprobaci�n");
			document.addAuthor("jchavarro");
			Chunk chunk = new Chunk("Carta de patrocinio", chapterFont);
			chunk.setBackground(BaseColor.GRAY);
			Chapter chapter = new Chapter(new Paragraph(chunk), 0);
			chapter.setNumberDepth(0);
			chapter.add(new Paragraph("\nNombres: " + persona.getNombres(), paragraphFont));
			chapter.add(new Paragraph("Apellidos: " + persona.getApellidos(), paragraphFont));
			chapter.add(new Paragraph("Cargo: " + persona.getCargo(), paragraphFont));
			chapter.add(new Paragraph("Correo: " + persona.getCorreo(), paragraphFont));
			chapter.add(new Paragraph("Direccion: " + persona.getDireccion(), paragraphFont));
			chapter.add(new Paragraph("Ciudad: " + persona.getCiudad(), paragraphFont));
			chapter.add(new Paragraph("Pais: " + persona.getPais(), paragraphFont));
			document.add(chapter);
			document.close();
			String[] lstDestinatarios = new String[1];
			lstDestinatarios[0] = persona.getCorreo();
			ec.enviarCorreo(lstDestinatarios, pc.getProperties("asunto"), pc.getProperties("contenido"));
			blnArchivoGenerado = true;
		} catch (DocumentException e) {
			System.out.println("Error generando el documento " + e.getMessage());
			e.printStackTrace();
		} catch (FileNotFoundException e) {
			System.out.println("Error archivo no encontrado " + e.getMessage());
			e.printStackTrace();
		} catch (Exception e) {
			System.out.println("Error enviar correo " + e.getMessage());
			e.printStackTrace();
		}
		return blnArchivoGenerado;
	}


}
