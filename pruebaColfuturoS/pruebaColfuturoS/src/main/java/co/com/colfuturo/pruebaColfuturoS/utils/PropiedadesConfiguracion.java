package co.com.colfuturo.pruebaColfuturoS.utils;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

public class PropiedadesConfiguracion {
	Properties properties = new Properties();

	/**
	 * Se obtiene el archivo properties.
	 * 
	 * @param nomPropiedad
	 * @return Objeto String.
	 */

	public String getProperties(String nomPropiedad) {
		try {

			if (properties.isEmpty()) {
				properties.load(new FileReader("c:\\tmp\\DataConfigProperties.prop"));
			}
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return properties.getProperty(nomPropiedad);
	}
}
