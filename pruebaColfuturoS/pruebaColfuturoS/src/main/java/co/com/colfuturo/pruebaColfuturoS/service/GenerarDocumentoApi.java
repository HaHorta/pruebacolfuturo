package co.com.colfuturo.pruebaColfuturoS.service;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import co.com.colfuturo.pruebaColfuturoS.dao.impl.Documento;
import co.com.colfuturo.pruebaColfuturoS.dto.PersonaDTO;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/colfuturo/")
public class GenerarDocumentoApi {

	@Autowired
	private Documento documento;

	@SuppressWarnings({ "rawtypes", "unchecked" })
	@RequestMapping(value = "generarDocumento", method = RequestMethod.POST)
	public ResponseEntity generarDocumento(@RequestBody PersonaDTO persona) {
		System.out.println("persona nombres " + persona.getNombres());
		boolean respuesta = documento.generarDocumento(persona);
		return new ResponseEntity(respuesta, HttpStatus.CREATED);
	}
}
