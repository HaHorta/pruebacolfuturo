package co.com.colfuturo.pruebaColfuturoS.dao;

import co.com.colfuturo.pruebaColfuturoS.dto.PersonaDTO;

public interface DocumentoDAO {

	public boolean generarDocumento( PersonaDTO persona);
	
}
