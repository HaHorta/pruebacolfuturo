package co.com.colfuturo.pruebaColfuturoS.dto;

public class PersonaDTO {

	private String nombres;
	private String apellidos;
	private String cargo;
	private String correo;
	private String direccion;
	private String detalles;
	private String ciudad;
	private String pais;
	
	public PersonaDTO() {
		super();
	}

	public PersonaDTO(String nombres, String apellidos, String cargo, String correo, String direccion,
			String detalles, String ciudad, String pais) {
		super();
		this.nombres = nombres;
		this.apellidos = apellidos;
		this.cargo = cargo;
		this.correo = correo;
		this.direccion = direccion;
		this.detalles = detalles;
		this.ciudad = ciudad;
		this.pais = pais;
	}

	public String getNombres() {
		return nombres;
	}

	public void setNombres(String nombres) {
		this.nombres = nombres;
	}

	public String getApellidos() {
		return apellidos;
	}

	public void setApellidos(String apellidos) {
		this.apellidos = apellidos;
	}

	public String getCargo() {
		return cargo;
	}

	public void setCargo(String cargo) {
		this.cargo = cargo;
	}

	public String getCorreo() {
		return correo;
	}

	public void setCorreo(String correo) {
		this.correo = correo;
	}

	public String getDireccion() {
		return direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	public String getDetalles() {
		return detalles;
	}

	public void setDetalles(String detalles) {
		this.detalles = detalles;
	}

	public String getCiudad() {
		return ciudad;
	}

	public void setCiudad(String ciudad) {
		this.ciudad = ciudad;
	}

	public String getPais() {
		return pais;
	}

	public void setPais(String pais) {
		this.pais = pais;
	}
	
	
}
