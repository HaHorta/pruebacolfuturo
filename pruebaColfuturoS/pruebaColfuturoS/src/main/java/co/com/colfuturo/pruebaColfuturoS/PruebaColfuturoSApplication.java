package co.com.colfuturo.pruebaColfuturoS;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan(basePackages = {"co.com.colfuturo.*"})
public class PruebaColfuturoSApplication {

	public static void main(String[] args) {
		SpringApplication.run(PruebaColfuturoSApplication.class, args);
	}

}
